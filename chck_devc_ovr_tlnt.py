#! /usr/bin/env python3
import ipaddress
import telnetlib
import os
import sys
import time
import re
from subprocess import Popen as exec_cmd
from subprocess import run, PIPE ,STDOUT
from os import system
import re
import pprint

def openShell(cmd):
    return exec_cmd([cmd],stdout=PIPE,stderr=STDOUT,shell=True).communicate()[0].decode("utf-8")

def openShellnoOut(cmd):
    return exec_cmd([cmd], shell=True).communicate()

def chooseAction(*args):
     

    def unpck(*args):
        # print(args)
        d = dict()
        d['ip_bypass'] = args[1]
        try:
            d['ip_jumper'] = args[2]
            return d
        except:
            d['ip_jumper'] = args[1]
            return d


    def tlnt():
        telnet.write(b'<BP01_1_R1_P_?>\n<BP01_1_R2_P_?>\n<BP01_2_R1_P_?>\n<BP01_2_R2_P_?>\n<BP01_3_R1_P_?>\n<BP01_3_R2_P_?>\n<BP01_4_R1_P_?>\n<BP01_4_R2_P_?>\n<BP01_5_R1_P_?>\n<BP01_5_R2_P_?>\n<BP01_6_R1_P_?>\n<BP01_6_R2_P_?>\n<BP01_7_R1_P_?>\n<BP01_7_R2_P_?>\n<BP01_8_R1_P_?>\n<BP01_8_R2_P_?>\n')
        time.sleep(5)
        all_result = telnet.read_very_eager().decode('utf-8')
        system('clear')
        pattern2 = r"BP01_|_P_"
        rere2 = re.sub(pattern2, ' ', all_result)
        pattern = r">"
        rere=re.sub(pattern, ' >\n', rere2).split('\n')
        lst=[]
        for i in rere:
            if '1_R1' in i:
                lst.append(i+' M1 L1 1_R1 | te2')
            elif '1_R2' in i:
                lst.append(i+' M1 L1 1_R2 | te1')
            elif '2_R1' in i:
                lst.append(i+' M1 L2 2_R1 | te4')
            elif '2_R2' in i:
                lst.append(i+' M1 L2 2_R2 | te3')
            elif '3_R1' in i:
                lst.append(i+' M2 L3 1_R1 | te6')
            elif '3_R2' in i:
                lst.append(i+' M2 L3 1_R2 | te5')
            elif '4_R1' in i:
                lst.append(i+' M2 L4 2_R1 | te8')
            elif '4_R2' in i:
                lst.append(i+' M2 L4 2_R2 | te7')
            elif '5_R1' in i:
                lst.append(i+' M3 L5 1_R1 | te10')
            elif '5_R2' in i:
                lst.append(i+' M3 L5 1_R2 | te9')
            elif '6_R1' in i:
                lst.append(i+' M3 L6 2_R1 | te12')
            elif '6_R2' in i:
                lst.append(i+' M3 L6 2_R2 | te11')
            elif '7_R1' in i:
                lst.append(i+' M4 L7 1_R1 | te14')
            elif '7_R2' in i:
                lst.append(i+' M4 L7 1_R2 | te13')
            elif '8_R1' in i:
                lst.append(i+' M4 L8 2_R1 | te16')
            elif '8_R2' in i:
                lst.append(i+' M4 L8 2_R2 | te15')
        return lst

    if ipaddress.ip_address(unpck(*args)['ip_jumper']).is_global:
        flag=True
        while True:
            try:
                print('trying to create tunnel...')
                sshnc = "sshpass -p 'SECRET' ssh -o ExitOnForwardFailure=yes -o StrictHostKeyChecking=no -f -N -L 4001:{}:4001 secret@{}".format(
                    unpck(*args)['ip_bypass'], unpck(*args)['ip_jumper'])
                openShellnoOut(sshnc)
                time.sleep(3)
                if 'failed:' in openShell('nc -zv 127.0.0.1 4001'):
                    print('trying to create tunnel...pls wait')
                    continue
                else:
                    break
            except:
                flag=False
                break
                print('TRY AGAIN , TUN WASNT ESTABLISED')
            if flag==False:
                break


        if 'succeeded!' in openShell('nc -zv 127.0.0.1 4001'):
            print('connecting telnet...')
            telnet = telnetlib.Telnet('127.0.0.1', port=4001)
            time.sleep(4)
            while True:
                try:
                    for i in tlnt():
                        print(i)
                    
                except:
                    telnet.close()
                    pid = openShell(
                        "lsof -i :4001 | awk '{print $2}'").split('\n')[1]
                    openShellnoOut('kill -9 {}'.format(pid))
                    break
        else:
                print('TRY AGAIN , TUN WASNT ESTABLISED')
    else:
        print('connecting telnet...')
        telnet = telnetlib.Telnet(unpck(*args)['ip_bypass'], port=4001)
        time.sleep(4)
        while True:
            try:
                for i in tlnt():
                    print(i)
            except:
                telnet.close()
                break

chooseAction(*sys.argv)
